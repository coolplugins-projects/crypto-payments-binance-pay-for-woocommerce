
var $ =jQuery;
var coin_list = $('#woocommerce_cbpw_select_currency option');
$(coin_list[1]).prop('disabled', 'disabled');
$(coin_list[2]).prop('disabled', 'disabled');


var choose_api = $('#woocommerce_cbpw_currency_conversion_api');
$('#woocommerce_cbpw_cbpw_merchantId').attr('required','required');
$('#woocommerce_cbpw_cbpw_public_key').attr('required', 'required');
$('#woocommerce_cbpw_cbpw_private_key').attr('required', 'required');
$('#woocommerce_cbpw_cbpw_openexchangerates_key').attr('required', 'required');
if ($(choose_api).val() == "cryptocompare") {
    $('#woocommerce_cbpw_cbpw_crypto_compare_key').parents('tr').show();
    $('#woocommerce_cbpw_cbpw_openexchangerates_key').parents('tr').hide();
} else {
    $('#woocommerce_cbpw_cbpw_crypto_compare_key').parents('tr').hide();
    $('#woocommerce_cbpw_cbpw_openexchangerates_key').parents('tr').show();
}
 choose_api.on('change', function () {
     if ($(this).val() == "cryptocompare") {
         $('#woocommerce_cbpw_cbpw_crypto_compare_key').parents('tr').show();
         $('#woocommerce_cbpw_cbpw_openexchangerates_key').parents('tr').hide();
     } else {
         $('#woocommerce_cbpw_cbpw_crypto_compare_key').parents('tr').hide();
         $('#woocommerce_cbpw_cbpw_openexchangerates_key').parents('tr').show();
     }
 });



$('.cbpw_modal-toggle').on('click', function (e) {
    e.preventDefault();
    $('.cbpw_modal').toggleClass('is-visible');
    var iBody = $("#cbpw_custom_cbpw_modal").contents().find("body");
    //  var updt_trgt = $(iBody).find('#plugin-information-footer a').attr('target', '_blank');
    var trget_link = $(iBody).find('#plugin-information-footer a').attr('href');
    // var chklink = (trget_link == "undefined") ?"Latest Version Installed":"";
    var adddat = $(iBody).find('#plugin-information-footer').html("<a data-slug='woocommerce' id='plugin_install_from_iframe' class='button button-primary right' href=" + trget_link + " target='_blank'>Install Now</a>")
    // console.log(trget_link)


});
