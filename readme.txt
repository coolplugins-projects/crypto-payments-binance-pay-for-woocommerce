=== Crypto Payments Binance Pay For Woocommerce ===
Contributors:narinder-singh,satindersingh,coolplugins
Donate link: https://paypal.me/CoolPlugins/10USD/
Tags: e-commerce, payment, crypto, BUSD, woocommerce
Requires at least:4.5
Tested up to:5.8
Requires PHP:5.6
Stable tag:1.0
License:GPLv2 or later
License URI:http://www.gnu.org/licenses/gpl-2.0.html

Binance pay enables customers to pay on your WooCommerce store.

== Description ==

Binance pay enables customers to pay with Binance pay on your WooCommerce store. Your customers will be offered
the option of paying with Binance pay at checkout. If they choose that option then they will be quoted a price
in Crypto currency for their order automatically.

After submitting their order they will be given the details of the Binance pay transaction they should make.

Features:

* Automatically convert order value to Crypto currency at checkout.
* Two price conversion API Support cryptocompare & Binance + Openexchangerates.
* Option to select default payment success status.
* Automatic payment confimation
* Scan and pay feature.
### 😎 Who's Behind

This plugin is not developed by or affiliated with "**Binace**".

**[🔗 Cool Plugins](https://coolplugins.net)** is a team of experienced WordPress plugin developers who manages this addon/plugin. Here are a few words about company:-

* 5+ years WordPress plugin development experience.
* 20+ free and premium WordPress plugins released.
* 1000000+ plugins downloads.
* 150K+ active websites are using our plugins.

> We provide cool solutions to extend the functionalities of famous WordPress plugins!

<h3>Third Party API & License Information</h3>	
* **API website:-** [https://developers.binance.com/](https://developers.binance.com/)	
* **API docs:-** [https://developers.binance.com/docs/binance-pay/app-integration](https://developers.binance.com/docs/binance-pay/app-integration)	
* **API website:-** [https://min-api.cryptocompare.com/](https://min-api.cryptocompare.com/)	
* **API docs:-** [https://min-api.cryptocompare.com/documentation](https://min-api.cryptocompare.com/documentation)	
* **API website:-** [https://openexchangerates.org/](https://openexchangerates.org/)	
* **API docs:-** [https://docs.openexchangerates.org/](https://docs.openexchangerates.org/)	
* **Privacy policy:-** [https://openexchangerates.org/privacy](https://openexchangerates.org/privacy)	
* **Term of Use:-** [https://openexchangerates.org/terms](https://openexchangerates.org/terms)	

== Installation ==
1. Install **Crypto Payments Binance Pay For Woocommercer** from the WordPress.org repository or by uploading plugin-zip unzipped folder to the **/wp-content/plugins** directory.
2. Activate the plugin through **Plugins >> Installed Plugin** menu in WordPress
3. After plugin activation, you can find the Binace payment gateway settings in the Woocommerce Payments Section.

== Changelog ==
<strong>Version 1.0 | 27 oct 2021</strong>
<pre>
New: Initial plugin release.
</pre>
