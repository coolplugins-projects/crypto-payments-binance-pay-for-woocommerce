<?php
/**
 * Plugin Name:Crypto Payments Binance Pay For Woocommerce
 * Description:Add Binance pay payment gateway for your woocommerce store.
 * Plugin URI:https://coolplugins.net/
 * Author:Cool Plugins
 * Author URI:https://coolplugins.net/
 * Version: 1.0
 * License: GPL2
 * Text Domain:CDBBC
 * Domain Path: /languages
 *
 * @package Binance_api_test
 */

/*
Copyright (C) 2018  CoolPlugins contact@coolplugins.net

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

if (!defined('ABSPATH')) {
    exit;
}
define('CBPW_VERSION', '1.0');
define('CBPW_FILE', __FILE__);
define('CBPW_PATH', plugin_dir_path(CBPW_FILE));
define('CBPW_URL', plugin_dir_url(CBPW_FILE));
/*** Cbpw_binance_pay main class by CoolPlugins.net */
if (!class_exists('Cbpw_binance_pay')) {
    final class Cbpw_binance_pay
    {
        /**
         * The unique instance of the plugin.
         *
         */
        private static $instance;

        /**
         * Gets an instance of our plugin.
         *
         */
        public static function get_instance()
        {
            if (null === self::$instance) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        /**
         * Constructor.
         */
        private function __construct()
        {

        }

        // register all hooks
        public function registers()
        {
            /*** Installation and uninstallation hooks */
            register_activation_hook(__FILE__, array('Cbpw_binance_pay', 'activate'));
            register_deactivation_hook(__FILE__, array('Cbpw_binance_pay', 'deactivate'));			
			add_action('plugins_loaded', array(self::$instance,'cbpw_load_files'));
            add_filter('woocommerce_payment_gateways', array(self::$instance, 'cbpw_add_gateway_class'));
            add_action('wp_ajax_nopriv_cbpw_order_status', 'cbpw_payment_verify');
            add_action('wp_ajax_cbpw_order_status', 'cbpw_payment_verify');		
            add_action('admin_enqueue_scripts', array($this, 'admin_style'));
            $this->cbpw_installation_date();

          
        }        
        function cbpw_installation_date(){
		 $get_installation_time = strtotime("now");
   	 	  add_option('cbpw_activation_time', $get_installation_time ); 
	    }
        public function admin_style($hook)
         {
        
        if($hook=="woocommerce_page_wc-settings"){
        wp_enqueue_script('cbpw-custom', CBPW_URL . 'assets/js/cbpw-admin.js', array('jquery'), CBPW_VERSION, true);
        }
        wp_enqueue_style('cbpw_admin_css', CBPW_URL . 'assets/css/cbpw-admin.css',array(), null, null, 'all');


         }
  
		function cbpw_add_gateway_class($gateways)
		{
			$gateways[] = 'WC_cbpw_Gateway'; // your class name is here
			return $gateways;
		}
        /*** Load required files */
        public function cbpw_load_files()
        {         
            if ( ! class_exists( 'WooCommerce' ) ) {
                add_action( 'admin_notices',array( $this,'cbpw_missing_wc_notice' ));
                return;
                }
            /*** Include helpers functions*/
			require_once CBPW_PATH . 'includes/binance-pay-api.php';
            require_once CBPW_PATH . 'includes/cbpw-woo-payment-gateway.php';     
            require_once CBPW_PATH . 'includes/cbpw-functions.php';     
            require_once CBPW_PATH . 'admin/class.review-notice.php';


        }
        function cbpw_missing_wc_notice()
        {
            $installurl = admin_url() . 'plugin-install.php?tab=plugin-information&plugin=woocommerce';   
                if (file_exists(WP_PLUGIN_DIR . '/woocommerce/woocommerce.php')) {
              //  $installurl = admin_url() . 'update.php?action=install-plugin&plugin=woocommerce&_wpnonce=e69e11f273';
                    echo '<div class="error"><p><strong>' .esc_html__('Crypto Payments Binance Pay For Woocommerce requires WooCommerce to be active', 'cryptapi') . '</div>';

            }
            else {
               //$installurl = admin_url() . 'update.php?action=install-plugin&plugin=woocommerce&_wpnonce=e69e11f273';
              // echo WP_PLUGIN_DIR . '/woocommerce/woocommerce.php';
                echo '<div class="error"><p><strong>' . sprintf(esc_html__('Crypto Payments Binance Pay For Woocommerce requires WooCommerce to be installed and active. You can download %s here.', 'cryptapi'), '<button class="cbpw_modal-toggle" > Install </button>') . '</strong></p></div>';
                  ?>
                <div class="cbpw_modal">
                    <div class="cbpw_modal-overlay cbpw_modal-toggle"></div>
                    <div class="cbpw_modal-wrapper cbpw_modal-transition">
                    <div class="cbpw_modal-header">
                        <button class="cbpw_modal-close cbpw_modal-toggle"><span class="dashicons dashicons-dismiss"></span></button>
                        <h2 class="cbpw_modal-heading"><?php _e("Install WooCommerce","cbpw")?></h2>
                    </div>
                    
                    <div class="cbpw_modal-body">
                        <div class="cbpw_modal-content">
                        <iframe  src="<?php echo esc_url($installurl); ?>" width="600" height="400" id="cbpw_custom_cbpw_modal"> </iframe>
                        </div>
                    </div>
                    </div>
                </div>
                <?php
            }
        }

        // set settings on plugin activation
        public static function activate()
        {
            update_option("cbpw-v", CBPW_VERSION);
            update_option("cbpw-type", "FREE");
            update_option("cbpw-installDate", date('Y-m-d h:i:s'));
            update_option("cbpw-already-rated", "no");
        }
        public static function deactivate()
        {
            
            delete_option("cbpw-v");
            delete_option("cbpw-type");
            delete_option("cbpw-installDate");
            delete_option("cbpw-already-rated");

        }


    }

}
/*** Cbpw_binance_pay main class - END */

/*** THANKS - CoolPlugins.net ) */
$cbpw = Cbpw_binance_pay::get_instance();
$cbpw->registers();

